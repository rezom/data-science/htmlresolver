import yargs from 'yargs';
import process from 'process';
import Promise from 'bluebird';
import readline from 'readline';
import jsdom from 'jsdom';

import vm2 from 'vm2';
const { NodeVM, VMScript, NodeVMOptions } = vm2;

/**
 * @type {NodeVMOptions}
 */
const DefaultNodeVMOptions = {
	require: {
		external: {
			modules: ['jsdom'],
		},
		root: './',
	},
};

const jsdomScript = new VMScript(`module.exports = require('jsdom');`);

const LOADTIMEOUT = 60000;
const OBSERVEDELAY = 80;

/**
 *
 * @param {String} cookieFileContents
 */
function parseCookieFileFormat(cookieFileContents) {
	return cookieFileContents
	.split('\n')
	.map(line => line.split("\t").map((word) => word.trim()))
	.filter(line => line.length === 7)
	.map(cookieData => ({
		key: cookieData[5],
		value: cookieData[6],
		domain: cookieData[0],
		crossDomain: cookieData[1] === 'TRUE',
		path: cookieData[2],
		https: cookieData[3] === 'TRUE',
		expire: ~~cookieData[4] ? ~~cookieData[4] : 0,
		httpOnly: false,
	}))
	.map(cookie => {
		if (cookie.domain.substr(0, 10) === "#HttpOnly_") {
			cookie.httpOnly = true;
			cookie.domain = cookie.domain.substr(10);
		}
		return cookie;
	});
}

class ExtraConfig {
	/**
	 * @param {string} userAgent
	 * @param {Object.<string,string[]>} headers
	 * @param {string} cookieFileContent
	 * @param {string} proxy
	 * @param {number} timeout
	 */
	constructor(userAgent, headers, cookieFileContent, proxy, timeout) {
		this.userAgent = userAgent;
		this.cookieFileContent = cookieFileContent;
		this.headers = headers;
		this.proxy = proxy;
		this.timeout = timeout;
	}

	/**
	 *
	 * @param {string} json
	 */
	static fromJSON(json) {
		if (!json || !json.length)
			return new ExtraConfig();
		const v = JSON.parse(json);
		return new ExtraConfig(
			v.userAgent,
			v.cookieFileContent,
			v.headers,
			v.proxy,
			v.timeout);
	}

	/**
	 *
	 * @param {Object.<string,any>} map
	 */
	static fromMap(map) {
		if (!map || !Object.keys(map).length)
			return new ExtraConfig();
		return new ExtraConfig(
			map.userAgent,
			map.cookieFileContent,
			map.headers,
			map.proxy,
			map.timeout);
	}
}

class ResolveTask {
	/**
	 *
	 * @param {string} id id of the task
	 * @param {string} type mimetype of the task
	 * @param {Date} start start time of the task
	 * @param {Date} end end time of the task
	 * @param {string} url url of the task
	 * @param {string} encoding encoding of the content
	 * @param {string} content content of the task
	 * @param {string[][]} log log of the task
	 * @param {Object.<string,any>} extra extra values of the task
	 */
	constructor(id, type, start, end, url, encoding, content, log, extra) {
		this.id = id;
		this.type = type;
		this.start = start;
		this.end = end;
		this.url = url;
		this.encoding = encoding;
		this.content = content;
		if (!log || !log.length || !Array.isArray(log)) {
			/** @type {string[][]} */
			this.log = [];
		} else {
			this.log = log;
		}
		this.extra = extra;
	}

	/**
	 *
	 * @param {string} json
	 */
	static fromJSON(json) {
		if (!json || !json.length)
			return new ResolveTask();
		const v = JSON.parse(json);
		return new ResolveTask(
			v.id,
			v.type,
			new Date(v.start),
			new Date(v.end),
			v.url,
			v.encoding,
			v.content,
			v.log,
			v.extra);
	}

	toJSON() {
		return JSON.stringify({
			id: this.id,
			type: this.type,
			start: this.start,
			end: this.end,
			url: this.url,
			encoding: this.encoding,
			content: this.content,
			log: this.log,
			extra: this.extra,
		});
	}

	getContentBuffer() {
		if (!this.content || !this.content.length)
			return null;
		if (this.encoding == 'base64')
			return Buffer.from(this.content, 'base64');
		return Buffer.from(this.content);
	}

	/**
	 *
	 * @param {Buffer} content
	 */
	setContentBuffer(content) {
		this.encoding = 'base64';
		this.content = content.toString('base64');
	}

	getExtraConfig() {
		return ExtraConfig.fromMap(this.extra);
	}

	/**
	 *
	 * @param {string} type
	 * @param {...any} args
	 */
	toLog(type, ...args) {
		if (!args || !args.length)
			return;
		const line = [ (new Date()).toISOString(), type ];
		let maxSize = 0;
		for (const val of args) {
			const sVal = String(val);
			if (!sVal.length) continue;
			if (sVal.length > maxSize) maxSize = sVal.length;
			line.push(sVal);
		}
		if (!maxSize) return;
		this.log.push(line);
	}
}

class Browser {
	constructor() {
		this.vm = new NodeVM({...DefaultNodeVMOptions, sandbox: {}});
		/**
		 * @type {jsdom}
		 */
		this.jsdom = this.vm.run(jsdomScript);
	}

	/**
	 *
	 * @param {ResolveTask} resolveTask
	 */
	jsdomConfig(resolveTask) {
		const extraConfig = resolveTask.getExtraConfig();
		/**
		 * @type {jsdom.ResourceLoaderConstructorOptions}
		 */
		const resourceLoaderConf = {strictSSL: false};
		if (extraConfig) {
			if (extraConfig.proxy && extraConfig.proxy.length)
				resourceLoaderConf.proxy = extraConfig.proxy
			if (extraConfig.userAgent && extraConfig.userAgent.length)
				resourceLoaderConf.userAgent = extraConfig.userAgent
		}
		const virtualConsole = new this.jsdom.VirtualConsole();
		virtualConsole.on('error', resolveTask.toLog.bind(resolveTask, 'error'));
		virtualConsole.on('info', resolveTask.toLog.bind(resolveTask, 'info'));
		virtualConsole.on('log', resolveTask.toLog.bind(resolveTask, 'log'));
		virtualConsole.on('warn', resolveTask.toLog.bind(resolveTask, 'warn'));
		virtualConsole.on('jsdomError', resolveTask.toLog.bind(resolveTask, 'jsdomError'));
		/**
		 * @type {jsdom.ConstructorOptions}
		 */
		const conf = {
			// asssume all tasks have utf-8 valid encoding
			contentType: 'text/html; charset=UTF-8',
			// hopefully vm2 can sandbox dangerous scripts
			runScripts: 'dangerously',
			resources: new this.jsdom.ResourceLoader(resourceLoaderConf),
			pretendToBeVisual: true,
			virtualConsole: virtualConsole,
		};
		if (resolveTask.url && resolveTask.url.length)
			conf.url = resolveTask.url;
		if (extraConfig) {
			if (extraConfig.userAgent && extraConfig.userAgent.length)
				conf.userAgent = extraConfig.userAgent
			if (extraConfig.headers) {
				// TODO : inject all headers
				let referer;
				for (const [key, value] of Object.entries(extraConfig.headers)) {
					if (key.toLowerCase() === 'referer') {
						if (Array.isArray(value)) {
							if (value.length)
								referer = value[0];
						} else {
							referer = value;
						}
						break;
					}
				}
				if (referer && referer.length) {
					conf.referrer = referer;
				}
			}
			if (extraConfig.cookieFileContent && extraConfig.cookieFileContent.length) {
				const cookieJar = new this.jsdom.CookieJar();
				const cookies = parseCookieFileFormat(extraConfig.cookieFileContent);
				for (const c of cookies) {
					const cookie = new this.jsdom.toughCookie.Cookie({
						key: c.key,
						value: c.value,
						domain: c.domain,
						path: c.path,
						secure: c.https,
						expires: new Date(c.expire * 1000),
						httpOnly: c.httpOnly,
						hostOnly: !c.crossDomain,
					});
					cookieJar.setCookieSync(cookie, conf.url);
				}
				conf.cookieJar = cookieJar;
			}
		}
		return conf;
	}

	/**
	 *
	 * @param {ResolveTask} resolveTask
	 */
	async load(resolveTask) {
		const content = resolveTask.getContentBuffer();
		if (!content || !content.length)
			return;
		let loadTimeout = LOADTIMEOUT;
		const extraConfig = resolveTask.getExtraConfig();
		if (extraConfig.timeout > 0)
			loadTimeout = extraConfig.timeout;
		let loadPromise = Promise.resolve();
		const startTime = new Date();
		const dom = new this.jsdom.JSDOM(content, {
			...this.jsdomConfig(resolveTask),
			beforeParse(window) {
				loadPromise = new Promise(res => window.addEventListener('load', res));
			},
		});
		loadTimeout -= (new Date()).getTime() - startTime.getTime();
		if (loadTimeout < 0) loadTimeout = 0;
		await loadPromise.timeout(loadTimeout, 'Load timeout reached').catch(() => {});
		let gRes = () => {};
		const observePromise = new Promise(res => { gRes = res; });
		let observeTimeout = setTimeout(gRes, OBSERVEDELAY);
		const observer = new dom.window.MutationObserver(() => {
			clearInterval(observeTimeout);
			observeTimeout = setTimeout(gRes, OBSERVEDELAY);
		});
		observer.observe(dom.window.document.documentElement, {
			attributeOldValue: true,
			attributes: true,
			characterData: true,
			characterDataOldValue: true,
			childList: true,
			subtree: true,
		});
		loadTimeout -= (new Date()).getTime() - startTime.getTime();
		if (loadTimeout < 0) loadTimeout = 0;
		await observePromise.timeout(loadTimeout).catch(() => {});
		observer.disconnect();
		resolveTask.encoding = null;
		resolveTask.content = dom.serialize();
		try { dom.window.close(); } catch (err) {}
	}
}

/**
 *
 * @param {string} line
 */
async function processLine(line) {
	if (line && line.length) {
		line = line.trim();
		if (line.length) {
			try {
				const resolveTask = ResolveTask.fromJSON(line);
				const browser = new Browser();
				await browser.load(resolveTask);
				process.stdout.write(resolveTask.toJSON(), 'utf8');
			}
			catch(err) { console.error(err); }
		}
	}
	process.stdout.write('\n', 'utf8');
}

async function main() {
	const argv = yargs(process.argv).argv;

	const readable = process.stdin;
	readable.setEncoding('utf8');
	let queuePromise = Promise.resolve();
	const rl = readline.createInterface(readable);
	rl.on('line', line => {
		queuePromise = queuePromise.finally(() => processLine(line));
	});
	rl.on('close', () => {
		queuePromise = queuePromise.finally(() => processLine(rl.line));
	});
}

main();
