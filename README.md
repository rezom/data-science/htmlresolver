# htmlresolver

This nodejs helper resolves html pages with dynamic javascript generation.
It can be fed tasks line by line on stdin or it can be connected to a light redis interface.

The intent is to stay as simple and minimalistic as possible for this helper.
The old jsresolver project had a lot of advanced things like child processes and workers with shared memory.

This one's job is to resolve a complete html page if there is heavy javascript use on it that generates the content. Any other task should be performed by the core datascience project.

## ResolveTask input format

```json
{
	"id": "unique id of the task",
	"mimetype": "text/html",
	"start": "2020-09-14T09:40:12.169Z",
	"end": "2020-09-14T09:40:12.169Z",
	"url": "https://example.net",
	"encoding": "null or base64",
	"content": "utf8 assumed content or base64 encoded",
	"log": [
		["2020-09-14T09:40:12.169Z", "log", "some logging"],
		["2020-09-14T09:40:12.169Z", "warn", "some more logging"]
	],
	"extra": {
		"userAgent": "some optional user-agent",
		"cookieFileContent": "optional cookieFile netscape file format to init a cookiejar",
		"headers": {
			"referer": ["https://example.net"]
		},
		"proxy": "optional http proxy to use for the requests",
		"timeout": 60000
	}
}
```

## Installation

```sh
yarn install
```

## Usage

### Stdin

The resolver can be fed tasks line by line :

```sh
echo '{"id":"test","type":"text/html","content":"<html><head><title>Nice title</title></head><body><script>const el = document.createElement(\"h1\"); el.appendChild(document.createTextNode(\"hello\")); document.body.appendChild(el);</script></body></html>"}' | \
	node index.mjs
```

## Potential problems

The older jsresolver project had transpilation and loop guard injection for foreign scripts to combat infinite loops.
To stay simple, this one does not have that security. Real world tests should be performed to assess the real impact.
It still has vm2 context protection for the jsdom instance.
